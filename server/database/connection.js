const mongoose = require('mongoose');
///funcion de coneccion con mongo db
const connectDB = async()=>{
    try {
        //conexion mongobd 
        const con = await mongoose.connect("mongodb+srv://admin:admin@cluster0.xoqpb.mongodb.net/newscover?retryWrites=true&w=majority", {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useFindAndModify: false,
            useCreateIndex: true
        });
        console.log(`Conexión con MongoDB: ${con.connection.name}`);
    } catch (error) {
        console.log(error);
        process.exit(1);
    }
}

module.exports = connectDB;