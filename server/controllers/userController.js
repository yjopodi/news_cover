///Import model user
const User = require('../models/userModel');
const Rol = require('../models/rolModel');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
 /**
  * Function to get user by id or list users
  * @param {id} req 
  * @param {Array:users} res 
  */
exports.get = (req,res) => {
    if(req.query.id){
        const id = req.query.id;

        User.findById(id)
            .then(data =>{
                if(!data){
                    res.status(404).send({ message : "Not found user with id "+ id})
                }else{
                    Rol.populate(data, { path: "rol"},function(err,data){
                        res.send(data)
                    })
                }
            })
            .catch(err =>{
                res.status(500).send({ message: "Erro retrieving user with id " + id})
            })

    }else{
        User.find({}, function(err, data){
        }).then(data => {
            Rol.populate(data, { path: "rol"},function(err,data){
                res.send(data)
            })
        }).catch(err => {
            res.status(500).send({ message : err.message || "Error Occurred while retriving user information" })
        })
    }
}
/**
 * Funtion to add user in the database
 * @param {email, first_name, last_name, password,perfil,rol} req 
 * @param {*} res 
 * @returns 
 */ 
exports.add = (req,res)=>{
    // validate request
    if(!req.body){
        res.status(400).send({ message : "Content can not be emtpy!"});
        return;
    }
    // new user
    const user = new User({
        email : req.body.email,
        first_name : req.body.first_name,
        last_name : req.body.last_name,
        password : req.body.password,
        perfil : req.body.perfil,
        rol : req.body.rol
    })
    ///Save user
    user.save(user)
        .then(data => {
            if(!data){
                res.status(404).send({ message : `Cannot insert user.`})
            }else{
                res.send(data)
            }  
        })
        .catch(err =>{
            res.status(500).send({
                message : err.message || "Some error occurred while creating a create operation"
            });
        });
}
/**
 * Funtion to update user
 * @param {id, first_name, last_name, password} req 
 * @param {*} res 
 * @returns 
 */
exports.update = (req, res)=>{
    if(!req.body){
        return res
            .status(400)
            .send({ message : "Data to update can not be empty"})
    }
    if(req.body.first_name === "" || req.body.last_name === "" || req.body.password === ""){
        res.status(500).send({ message : "Error Update user information"})
    }
    const id = req.body.id;
    User.findByIdAndUpdate(id, req.body, { useFindAndModify: false})
        .then(data => {
            if(!data){
                res.status(404).send({ message : `Cannot Update user with ${id}. Maybe user not found!`})
            }else{
                res.send(data)
            }
        })
        .catch(err =>{
            res.status(500).send({ message : "Error Update user information"})
        })
}
/**
 * Function to delete user
 * @param {id} req 
 * @param {*} res 
 */
exports.delete = async (req, res)=>{
    const id = req.body.id;

    User.findByIdAndDelete(id)
        .then(data => {
            if(!data){
                res.status(404).send({ message : `Cannot Delete with id ${id}.`})
            }else{
                res.send({
                    message : "User was deleted successfully!"
                })
            }
        })
        .catch(err =>{
            res.status(500).send({
                message: "Could not delete user with id=" + id
            });
        });
}
/**
 * Funtion to register new user
 * @param {email, first_name, last_name, password,perfil,rol} req 
 * @param {array[user]} res 
 * @param {*} next 
 */
exports.register = (req, res, next) =>{
    bcrypt.hash(req.body.password, 10, function(err, hashedPass){
        if(err){
            res.send({ message : 'Error'})
        }
        const user = new User({
            email : req.body.email,
            first_name : req.body.first_name,
            last_name : req.body.last_name,
            password : hashedPass,
            perfil : req.body.perfil,
            rol : req.body.rol
        })
        user.save(user)
        .then(data => {
            if(!data){
                res.status(404).send({ message : `Cannot insert user.`})
            }else{
                res.send(data)
            }  
        })
        .catch(err =>{
            res.status(500).send({
                message : err.message || "Some error occurred while creating a create operation"
            });
        });
    })
} 
/**
 * Funtion for authentication user
 * @param {username,password} req 
 * @param {array[user]} res 
 */
exports.authenticate = (req, res) =>{
    let username = req.body.username
    let password = req.body.password
    User.findOne({
        $or :[{email:username}, {first_name:username}]
    }).then(user => {
        if(user){
            bcrypt.compare(password, user.password, function(err, result){
                if(err){
                    res.send({ message : err.message || 'Error authenticate'})
                }
                if(result){
                    //let token = jwt.sign({name: user.name}, 'verySecretValue', {expiresIn: '1h'})
                    res.status(200).send({message: 'Login successfully', user})
                }
                else {
                    res.send({message: 'Password does not matched'})
                }
            })
        }else{
            res.send({ message : 'No user found'})
        }  
    })
    .catch(err =>{
        res.status(500).send({
            message : err.message || "Some error occurred while creating a create operation"
        });
    });
}