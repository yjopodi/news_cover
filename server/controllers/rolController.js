///Importacion del modelo rol
const Rol = require('../models/rolModel');
 /**
  * Funcion para obtener los roles de usuario por id o una listado de todos
  * @param {id} req 
  * @param {Array:roles} res 
  */
exports.obtener = (req,res) => {
    if(req.query.id){
        const id = req.query.id;

        Rol.findById(id)
            .then(data =>{
                if(!data){
                    res.status(404).send({ message : "Not found rol with id "+ id})
                }else{
                    res.send(data)
                }
            })
            .catch(err =>{
                res.status(500).send({ message: "Erro retrieving rol with id " + id})
            })

    }else{
        Rol.find()
            .then(data => {
                res.send(data)
            })
            .catch(err => {
                res.status(500).send({ message : err.message || "Error Occurred while retriving rol information" })
            })
    }
}
/**
 * Funcion para agregar roles a la base de datos
 * @param {name} req 
 * @param {*} res 
 * @returns 
 */
exports.agregar = async(req,res)=>{
    // validate request
    if(!req.body){
        res.status(400).send({ message : "Content can not be emtpy!"});
        return;
    }
    // new rol
    const rol = new Rol({
        name : req.body.name
    })
    ///Save rol
    rol.save(rol)
        .then(data => {
            //res.send(data)
            res.redirect('/');
        })
        .catch(err =>{
            res.status(500).send({
                message : err.message || "Some error occurred while creating a create operation"
            });
        });


}
/**
 * Funcion para editar un rol especificado por id
 * @param {id,name} req 
 * @param {*} res 
 * @returns 
 */
exports.editar = (req, res)=>{
    if(!req.body){
        console.log(req.body);
        return res
            .status(400)
            .send({ message : "Data to update can not be empty"})
    }

    const id = req.body.id;
    Rol.findByIdAndUpdate(id, req.body, { useFindAndModify: false})
        .then(data => {
            if(!data){
                res.status(404).send({ message : `Cannot Update rol with ${id}. Maybe course not found!`})
            }else{
                res.send(data)
            }
        })
        .catch(err =>{
            res.status(500).send({ message : "Error Update rol information"})
        })
}
/**
 * Funcion que eliminada un rol por medio del id
 * @param {id} req 
 * @param {*} res 
 */
exports.eliminar = async (req, res)=>{
    const id = req.body.id;

    Rol.findByIdAndDelete(id)
        .then(data => {
            if(!data){
                res.status(404).send({ message : `Cannot Delete with id ${id}.`})
            }else{
                res.send({
                    message : "Rol was deleted successfully!"
                })
            }
        })
        .catch(err =>{
            res.status(500).send({
                message: "Could not delete rol with id=" + id
            });
        });
}