///Import model news
const News = require('../models/newsModel');
const Source = require('../models/sourceModel')
const User = require('../models/userModel');
const Category = require('../models/categoryModel');
const Parser = require('rss-parser');
 /**
  * Function to get user by id or list news
  * @param {id} req 
  * @param {Array:news} res 
  */                                                       
exports.get = (req,res) => {
    if(req.body.id){
        const id = req.body.id;
        News.findById(id)
            .then(data =>{
                if(!data){
                    res.status(404).send({ message : "Not found news with id "+ id})
                }else{
                    User.populate(data, { path: "user"},function(err,data){
                        Category.populate(data, {path:"category"}, function(err,data){
                            res.send(data)
                        })
                    })
                }
            })
            .catch(err =>{
                res.status(500).send({ message: "Error retrieving news with id " + id})
            })

    }else{
        News.find().sort({date:-1})
            .then(data => {
                User.populate(data, { path: "user"},function(err,data){
                    Category.populate(data, {path:"category"}, function(err,data){
                        res.send(data)
                    })
                })
            })
            .catch(err => {
                res.status(500).send({ message : err.message || "Error Occurred while retriving news information" })
            })
    }
}
/**
 * Funtion to add news in the database
 * @param {title,short_description,permanlink,image_url, date,user,category} req 
 * @param {*} res 
 * @returns 
 */
exports.add = async(req,res)=>{
    // validate request
    if(!req.body){
        res.status(400).send({ message : "Content can not be emtpy!"});
        return;
    }
    // new news
    const news = new News({
        title : req.body.title,
        short_description : req.body.short_description,
        permanlink : req.body.permanlink,
        image_url : req.body.image_url,
        date : new Date(),
        user : req.body.user,
        category : req.body.category
    })
    ///Save news
    news.save(news)
        .then(data => {
            //res.send(data)
            res.redirect('/');
        })
        .catch(err =>{
            res.status(500).send({
                message : err.message || "Some error occurred while creating a create operation"
            });
        });


}
/**
 * Funtion to update news
 * @param {url, name, category, user} req 
 * @param {*} res 
 * @returns 
 */
exports.update = (req, res)=>{
    if(!req.body){
        return res
            .status(400)
            .send({ message : "Data to update can not be empty"})
    }
    if(req.body.url && req.body.name === "" || req.body.category && req.body.user === ""){
        res.status(500).send({ message : "Error Update news information"})
    }
    const id = req.body.id;
    News.findByIdAndUpdate(id, req.body, { useFindAndModify: false})
        .then(data => {
            if(!data){
                res.status(404).send({ message : `Cannot Update news with ${id}. Maybe news not found!`})
            }else{
                res.send(data)
            }
        })
        .catch(err =>{
            res.status(500).send({ message : "Error Update news information"})
        })
}
/**
 * Function to delete news 
 * @param {id} req 
 * @param {*} res        
 */
exports.delete = async (req, res)=>{
    const id = req.body.id;

    News.findByIdAndDelete(id)
        .then(data => {
            if(!data){
                res.status(404).send({ message : `Cannot Delete with id ${id}.`})
            }else{
                res.send({
                    message : "News was deleted successfully!"
                })
            }
        })
        .catch(err =>{
            res.status(500).send({
                message: "Could not delete news with id=" + id
            });
        });
}
/**
 * Function to store news
 * @param {object news} req 
 * @param {status 200} res 
 */
exports.addNews = (req, res) =>{
    const id = req.body.id;
    const user_id = req.params.user;
    Source.findById(id).then(data =>{
        Category.populate(data, {path:"category"}, function(err,data){
            const save = saveNews(data, user_id);
            if(save){
                res.status(200).send({
                    message: "Save news successfully"
                });
            }
        })
    })
    .catch(error =>{
        res.status(500).send({
            message: "Could not delete news with id=" + id
        });
    })
}

let saveNews = async (data, user_id) => {
    let parser = new Parser();
    let feed = await parser.parseURL(data.url);
    
    feed.items.forEach(item => { 
        if(data.category.name == item.categories[0]){
            //console.log(item);
            News.insertMany([{
                title : item.title,
                short_description : item.contentSnippet,
                permanlink : item.link,
                image_url : "https://extyseg.com/wp-content/uploads/2019/04/EXTYSEG-imagen-no-disponible.jpg",
                date : item.pubDate,
                user : user_id,
                category : data.category._id
        }])
        }
    })
}
/**
 * Function that gets the news according to the user
 * @param {id_user} req 
 * @param {Array[news]} res 
 */
exports.getNewsByUser = (req, res)=>{
    const user_id = req.params.user;
    News.find({user: user_id}).sort({date:-1})
    .then(data => {
        User.populate(data, { path: "user"},function(err,data){
            Category.populate(data, {path:"category"}, function(err,data){
                res.send(data)
            })
        })
    })
    .catch(err => {
        res.status(500).send({ message : err.message || "Error Occurred while retriving news information" })
    }) 
}

/**
 * Function to get the news according to the category
 * @param {id_category,id_user} req 
 * @param {Array[noticias]} res 
 */
exports.getNewsByCategory = (req, res)=>{
    const category_id = req.params.category;
    const user_id = req.params.user;
    News.find({category: category_id, user: user_id}).sort({date:-1})
    .then(data => {
        User.populate(data, { path: "user"},function(err,data){
            Category.populate(data, {path:"category"}, function(err,data){
                res.send(data)
            })
        })
    })
    .catch(err => {
        res.status(500).send({ message : err.message || "Error Occurred while retriving news information" })
    }) 
}