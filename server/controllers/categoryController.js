///Import model category
const Category = require('../models/categoryModel');
 /**
  * Function to get list categories
  * @param {id} req 
  * @param {Array:categories} res 
  */
exports.get = (req,res) => {
    if(req.query.id){
        const id = req.query.id;
        Category.findById(id)
            .then(data =>{
                if(!data){
                    res.status(404).send({ message : "Not found category with id "+ id})
                }else{
                    res.send(data)
                }
            })
            .catch(err =>{
                res.status(500).send({ message: "Erro retrieving user with id " + id})
            })

    }else{
        Category.find({}, function(err, data){
        }).then(data => {
            res.send(data)
        }).catch(err => {
            res.status(500).send({ message : err.message || "Error Occurred while retriving category information" })
        })
    }
}
/**
 * Funtion to add category in the database
 * @param {name} req 
 * @param {*} res 
 * @returns 
 */
exports.add = async(req,res)=>{
    // validate request
    if(!req.body){
        res.status(400).send({ message : "Content can not be emtpy!"});
        return;
    }
    // new category
    const category = new Category({
        name : req.body.name
    })
    ///Save category
    category.save(category) 
        .then(data => {
            if(!data){
                res.status(404).send({ message : `Cannot insert category.`})
            }else{
                res.send(data)
            }
        })
        .catch(err =>{
            res.status(500).send({
                message : err.message || "Some error occurred while creating a create operation"
            });
        });


}
/**
 * Function to update category
 * @param {id, name} req 
 * @param {*} res 
 * @returns 
 */
exports.update = (req, res)=>{
    if(!req.body){
        return res
            .status(400)
            .send({ message : "Data to update can not be empty"})
    }
    if(req.body.name === ""){
        res.status(500).send({ message : "Error Update category information"})
    }
        const id = req.body.id;
    Category.findByIdAndUpdate(id, req.body, { useFindAndModify: false})
        .then(data => {
            if(!data){
                res.status(404).send({ message : `Cannot Update category with ${id}. Maybe category not found!`})
            }else{
                res.send(data)
            }
        })
        .catch(err =>{
            res.status(500).send({ message : "Error Update category information"})
        })
}
/**
 * Function to delete category
 * @param {id} req 
 * @param {*} res 
 */ 
exports.delete = async (req, res)=>{
    const id = req.params.id;
    Category.findByIdAndDelete(id)
        .then(data => {
            if(!data){
                res.status(404).send({ message : `Cannot Delete with id ${id}.`})
            }else{
                res.send({
                    message : "Category was deleted successfully!"
                })
            }
        }) 
        .catch(err =>{
            res.status(500).send({
                message: "Could not delete user with id=" + id
            });
        });
}