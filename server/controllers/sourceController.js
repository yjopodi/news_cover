//Import model source
const Source = require('../models/sourceModel');
const User = require('../models/userModel');
const Category = require('../models/categoryModel');
const Parser = require('rss-parser');

const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;

/**
  * Function to get user by id or list sources
  * @param {id} req 
  * @param {Array:sources} res 
  */
exports.get = (req,res) => {
    if(req.query.id){
        const id = req.query.id;
        Source.findById(id)
            .then(data =>{
                if(!data){
                    res.status(404).send({ message : "Not found source with id "+ id})
                }else{
                    Category.populate(data, { path: "category"},function(err,data){
                        User.populate(data, {path:"user"}, function(err,data){
                            res.send(data)
                        })
                    })
                }
            })
            .catch(err =>{
                res.status(500).send({ message: "Error retrieving source with id " + id})
            })

    }else{
        Source.find({}, function(err, data){
        }).then(data => {
                Category.populate(data, { path: "category"},function(err,data){
                    User.populate(data, {path:"user"}, function(err,data){
                        res.send(data)
                    })
                })
            })
            .catch(err => {
                res.status(500).send({ message : err.message || "Error Occurred while retriving source information" })
            })
    }
}
/**
 * Funtion to add source in the database
 * @param {url, name, category, user} req 
 * @param {*} res 
 * @returns 
 */
exports.add = async(req,res)=>{
    // validate request
    if(!req.body){
        res.status(400).send({ message : "Content can not be emtpy!"});
        return;
    }
    // new source
    if(req.body.url === "" || req.body.name ===""){
        res.status(400).send({ message : "Content can not be emtpy!"});
    }
    const source = new Source({
        url : req.body.url,
        name : req.body.name,
        category : req.body.category,
        user : req.body.user
    })
    ///Save source
    source.save(source)
        .then(data => {
            if(!data){
                res.status(404).send({ message : `Cannot insert source.`})
            }else{
                res.send(data)
            }
        })
        .catch(err =>{
            res.status(500).send({
                message : err.message || "Some error occurred while creating a create operation"
            });
        });


}
/**
 * Funtion to update source
 * @param {url, name, category, user} req 
 * @param {*} res 
 * @returns 
 */
exports.update = (req, res)=>{
    if(!req.body){
        return res
            .status(400)
            .send({ message : "Data to update can not be empty"})
    }
    if(req.body.url && req.body.name === "" || req.body.category && req.body.user === ""){
        res.status(500).send({ message : "Error Update source information"})
    }
    const id = req.body.id;
    Source.findByIdAndUpdate(id, req.body, { useFindAndModify: false})
        .then(data => {
            if(!data){
                res.status(404).send({ message : `Cannot Update source with ${id}. Maybe source not found!`})
            }else{
                res.send(data)
            }
        })
        .catch(err =>{
            res.status(500).send({ message : "Error Update source information"})
        })
}
/**
 * Function to delete source
 * @param {id} req 
 * @param {*} res                       user
 */
exports.delete = async (req, res)=>{
    const id = req.params.id;
    Source.findByIdAndDelete(id)
        .then(data => {
            if(!data){
                res.status(404).send({ message : `Cannot Delete with id ${id}.`})
            }else{
                res.send({
                    message : "Source was deleted successfully!"
                })
            }
        })
        .catch(err =>{
            res.status(500).send({
                message: "Could not delete source with id=" + id
            });
        });
}

/**
 * Function for get sources by user id 
 * @param {user_id} req 
 * @param {array[source]} res 
 */
exports.getSourceByUser = async (req, res) =>{
    if(req.query.id){
        const id = req.query.id;
        const result = await Source.aggregate(
            [
                {
                    $match: 
                        {
                            user: ObjectId(id)
                        }
                },
                {
                    $lookup:
                        {
                            from: "categories",
                            localField: "category",
                            foreignField: "_id",
                            as: "category_details"
                        }
                }
            ]
        )
        res.status(200).send(result);
    }else {
        res.status(500).send({ message: "Error retrieving source with id " + id})
    }
}
/**
 * Funtion for get sources uniques(no used)
 * @param {*} req 
 * @param {*} res 
 */
exports.getSourceUrlGroup = async (req, res) =>{
    const result = await Source.aggregate(
    [
        {
            $group:{
                _id: "$url"
            }
        }
    ]
) 
    res.status(200).send(result);   
}

/**
  * Function to get user by id or list sources
  * @param {id} req 
  * @param {Array:sources} res 
  */
 exports.getCategorySource = (req,res) => {
    if(req.params.user){
        const id = req.params.user;
        Source.find({user: id})
            .then(data =>{
                if(!data){
                    res.status(404).send({ message : "Not found source with id "+ id})
                }else{
                    Category.populate(data, { path: "category"},function(err,data){
                        User.populate(data, {path:"user"}, function(err,data){
                            res.send(data)
                        })
                    })
                }
            })
            .catch(err =>{
                res.status(500).send({ message: "Error retrieving source with id " + id})
            })

    }else{
        Source.find({}, function(err, data){
        }).then(data => {
                Category.populate(data, { path: "category"},function(err,data){
                    User.populate(data, {path:"user"}, function(err,data){
                        res.send(data)
                    })
                })
            })
            .catch(err => {
                res.status(500).send({ message : err.message || "Error Occurred while retriving source information" })
            })
    }
}
