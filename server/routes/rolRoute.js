const { Router } = require("express");
const routerRol = Router();
const rolC = require("../controllers/rolController");

//Rutas
routerRol.get('/', rolC.obtener);
routerRol.post('/', rolC.agregar);
routerRol.put('/', rolC.editar);
routerRol.delete('/', rolC.eliminar);

module.exports = routerRol;