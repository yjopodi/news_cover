const { Router } = require("express");
const routerSource = Router();
const sourceC = require("../controllers/sourceController");

//Routes
routerSource.get('/', sourceC.get);
routerSource.post('/', sourceC.add);
routerSource.put('/', sourceC.update);
routerSource.delete('/:id', sourceC.delete);
routerSource.get('/user', sourceC.getSourceByUser);
routerSource.get('/url', sourceC.getSourceUrlGroup);
routerSource.get('/:user', sourceC.getCategorySource);
module.exports = routerSource;