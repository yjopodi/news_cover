const { Router } = require("express");
const routerUser = Router();
const userC = require("../controllers/userController");
//Routes
routerUser.get('/', userC.get);
routerUser.post('/', userC.add);
routerUser.put('/', userC.update);
routerUser.delete('/', userC.delete);
routerUser.post('/register', userC.register);
routerUser.post('/login', userC.authenticate);

module.exports = routerUser;