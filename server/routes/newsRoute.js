const { Router } = require("express");
const routerNews = Router();
const newsC = require("../controllers/newsController");

//Routes
routerNews.post('/:user', newsC.addNews);
routerNews.get('/:user', newsC.getNewsByUser);
routerNews.get('/:category/:user', newsC.getNewsByCategory);
routerNews.put('/', newsC.update);
routerNews.delete('/', newsC.delete);

module.exports = routerNews;