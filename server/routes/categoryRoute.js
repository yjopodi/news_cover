const { Router } = require("express");
const routerCategory = Router();
const categoryC = require("../controllers/categoryController");

//Rutas
routerCategory.get('/', categoryC.get);
routerCategory.post('/', categoryC.add);
routerCategory.put('/', categoryC.update);
routerCategory.delete('/:id', categoryC.delete);

module.exports = routerCategory;