const mongoose = require('mongoose');

var schema = new mongoose.Schema({
    name : {
        type : String,
        require: true
    }
})

const Categorydb = mongoose.model('Category', schema);

module.exports = Categorydb;