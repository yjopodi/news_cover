const mongoose = require('mongoose');

var schema = new mongoose.Schema({
    name : {
        type : String,
        require: true
    }
})

const roldb = mongoose.model('Rol', schema);

module.exports = roldb;