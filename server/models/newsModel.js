const mongoose = require('mongoose');
var categoryS = mongoose.Schema;
var userS = mongoose.Schema;
var schema = new mongoose.Schema({
    title : {
        type : String,
        require: true
    },
    short_description : {
        type : String,
        require: true
    },
    permanlink : {
        type : String,
        require: true
    },
    image_url : {
        type : String,
        require: true
    },
    date : {
        type : Date,
        require: true
    },
    user : {
        type : userS.ObjectId,ref: "User",
        require: true
    },
    category : {
        type : categoryS.ObjectId,ref: "Category",
        require: true
    }
    
})

const Sourcedb = mongoose.model('News', schema);

module.exports = Sourcedb;