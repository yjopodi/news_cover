const mongoose = require('mongoose');
//var rolS = mongoose.Schema;
let schema = new mongoose.Schema({
    email : {
        type : String,
        require: true
    },
    first_name : {
        type : String,
        require: true
    },
    last_name : {
        type : String,
        require: true
    },
    password : {
        type : String,
        require: true
    },
    perfil : {
        type : Boolean,
        require: true,
        default: true
    },
    rol : {
        type : String,
        require: true,
        default: "client"
    }
})

const Userdb = mongoose.model('User', schema);

module.exports = Userdb;