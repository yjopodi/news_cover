const mongoose = require('mongoose');
var categoryS = mongoose.Schema;
var userS = mongoose.Schema;
var schema = new mongoose.Schema({
    url : {
        type : String,
        require: true
    },
    name : {
        type : String,
        require: true
    },
    category : {
        type : categoryS.ObjectId,ref: "Category",
        require: true
    },
    user : {
        type : userS.ObjectId,ref: "User",
        require: true
    }
})

const Sourcedb = mongoose.model('Source', schema);

module.exports = Sourcedb;