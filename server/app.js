//Require
const express = require("express");
const cron = require('node-cron');
const morgan = require("morgan");
const cors = require("cors");
const conexionDB = require("./database/connection");
//Imports routes
const routerRol = require("./routes/rolRoute");
const routerUser = require("./routes/userRoute");
const routerSource = require("./routes/sourceRoute");
const routerCategory = require("./routes/categoryRoute");
const routerNews = require("./routes/newsRoute");

const app = express();

//Conexion con la DB
conexionDB();
// settings
app.set("name", "my-news-cover");
app.set("port", 3001);

//Middleware
app.use(express.json());
app.use(morgan("dev"));

// check for cors
app.use(cors({
    domains: '*',
    methods: "*"
  }));

//Archivo estatico de pagina de inicio presentar del api
app.use("/",express.static(__dirname +'/public'));

//Definicion de la ruta de entrada especificando

//Rols
app.use("/api/rols", routerRol);

//Users
app.use("/api/users", routerUser);

//Categories
app.use("/api/categories", routerCategory);

//Source
app.use("/api/sources", routerSource);

//News
app.use("/api/news", routerNews);

module.exports = app;