let Parser = require('rss-parser');
let parser = new Parser();

(async save(){

    let feed = await parser.parseURL('https://www.lanacion.com.ar/arcio/rss/');
    
    console.log(feed.title);

    feed.items.forEach(item => {
		    console.log(item.title + "\n"+
            item.description + "\n"+
            item.link + "\n"+
            item.guid + "\n"+
            item.pubDate +"\n\n\n\n");
    });
 
})();
